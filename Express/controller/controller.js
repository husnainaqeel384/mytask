const knex = require('../model/db');

const userdata  = {
  
    async adduserdetails(req,res){
   
     const title = req.body.title;
    
     const description = req.body.description;
     

        const added = await knex('userdata').select( 'TITLE', 'DESCRIPTION').insert({  TITLE:title, DESCRIPTION:description});
        res.status(201).json({
            success: true,
            added
        })

    },
    async display(req,res){
       const list= await knex('userdata').select('*');
        res.status(200).json({
            success: true,
           list
        })
    }

}

module.exports = userdata
