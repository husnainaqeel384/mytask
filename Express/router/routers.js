const express = require('express');
const router = express.Router();

const controller =require('../controller/controller')
// router of add data
router.route("/add").get(controller.adduserdetails);

// route of display the list
router.route("/show").get(controller.display);
module.exports = router;