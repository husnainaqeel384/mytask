const express = require('express');

const app = express();
const bodyParser= require('body-parser');
const port = 4000;

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
const user = require('./router/routers');
app.use("/v1",user);

app.listen(port,  () =>{
    console.log(`Server is working on ${port}`);
})
